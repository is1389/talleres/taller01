#include <stdio.h>

int main(void){
	unsigned num;		// Declaro variable entera positiva num.
	unsigned temp; 		// Declaro variable entera positiva temp.
	unsigned sum = 0;	// Declaro variable entera positiva sum y asigno valor 0.

	printf("Ingresa un número (hasta 5 cifras): ");	// Pregunto a usuario número de hasta 5 cifras.
	scanf("%5u", &num);				// Obtengo de usuario número y asigno a variable num.

	temp = num; // Asigno valor de num a temp.

	sum += temp % 10;	// Sumo el módulo 10 de temp a sum.
	temp /= 10;		// Asigno temp dividido en 10 a temp.
	sum += temp % 10;	// Sumo el módulo 10 de temp a sum.
	temp /= 10;		// Asigno temp dividido en 10 a temp.
	sum += temp % 10;	// Sumo el módulo 10 de temp a sum.
	temp /= 10;		// Asigno temp dividido en 10 a temp.
	sum += temp % 10;	// Sumo el módulo 10 de temp a sum.
	temp /= 10;		// Asigno temp dividido en 10 a temp.
	sum += temp % 10;	// Sumo el módulo 10 de temp a sum.

	printf("Suma de cifras de %u es %u.\n", num, sum);	// Imprimo variable num y sum.

	return 0;
}
