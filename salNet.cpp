#include <iostream>

using namespace std;

int main(void){
	unsigned short horas;	// Declaro variable entera positiva horas.
	unsigned costo;		// Declaro variable entera positiva costo.
	unsigned short visitas;	// Declaro variable entera positiva visitas.

	cout << "Ingresa horas trabajadas: ";	// Pregunto a usuario por cantidad de horas trabajadas.
	cin >> horas;				// Obtengo de usuario un número entero positivo y asigno a variable horas.
	cout << "Ingresa costo de hora: ";	// Pregunto a usuario por costo de hora.
	cin >> costo;				// Obtengo de usuario un número entero positivo y asigno a variable costo.
	cout << "Ingresa visitas: ";		// Pregunto a usuario por costo de hora.
	cin >> visitas;				// Obtengo de usuario un número entero positivo y asigno a variable visitas.

	unsigned salBru = horas * costo;	// Declaro variable entera positiva salBru y asigno la multiplicación entre horas y costo.
	unsigned salNet = salBru;		// Declaro variable entera positiva salNet y asigno el valor de salBru.

	salNet -= salBru * 0.045;		// Resto a salNet el 4.5% de salBru (salud).
	salNet -= salBru * 0.035;		// Resto a salNet el 3.5% de salBru (pensión).
	salNet -= 5000 * visitas;		// Resto a salNet 5000 * visitas (caja a compensación).
	salNet += salBru * 0.05;		// Sumo a salNet el 5% de salBru (salud).

	cout << "Salario neto: " << salNet << endl;	// Imprimo en pantalla el valor de salNet.

	return 0;
}
