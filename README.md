# 1. Suma de dígitos

Escribe un programa que obtiene un número entero de hasta 5 cifras, extrae cada
uno de los dígitos e imprime en pantalla la suma entre estos[^1]. Ejemplo:

```bash
$ ./sumDig.out
Ingresa un número (hasta 5 cifras): 12345
Suma de cifras de 12345 es 15.
$ ./sumDig.out
Ingresa un número (hasta 5 cifras): 4096
Suma de cifras de 4096 es 19.
```

> **Pista:** usa el operador `%`.

> **Nota:** no usar ciclos.

## Refinamiento 1

1. Preguntar y obtener del usuario número de hasta `5` cifras.
1. Extraer y sumar cada una de las `5` cifras.
1. Imprimir la suma de las cifras.

## Refinamiento 2

1. Declaro variable entera positiva `num`.
1. Declaro variable entera positiva `temp`.
1. Declaro variable entera positiva `sum` y asigno valor `0`.
1. Pregunto a usuario número de hasta `5` cifras.
1. Obtengo de usuario número y asigno a variable `num`.
1. Asigno valor de `num` a `temp`.
1. Sumo el módulo `10` de `temp` a `sum`.
1. Asigno `temp` dividido en `10` a `temp`.
1. Sumo el módulo `10` de `temp` a `sum`.
1. Asigno `temp` dividido en `10` a `temp`.
1. Sumo el módulo `10` de `temp` a `sum`.
1. Asigno `temp` dividido en `10` a `temp`.
1. Sumo el módulo `10` de `temp` a `sum`.
1. Asigno `temp` dividido en `10` a `temp`.
1. Sumo el módulo `10` de `temp` a `sum`.
1. Imprimo variable `num` y `sum`.


# 2. Salario neto

Juan  ha  trabajado  un  número  de  horas  en  la  semana  y  se  ha convenido
con  él, pagarle una determinada suma de dinero por cada hora trabajada. Por
ley, Juan debe afiliarse a alguna EPS para tener cubierto los riesgos de salud.
El aporte a los sistemas de salud es el `13.5%` del salario bruto,  del  cual
el empleador  cubre  las  `2/3`  partes  y  el  empleado  `1/3` parte.

El  aporte  para pensiones  es  del  `10.5%`  del  salario  bruto  que se
reparte  entre  empleador  y  empleado  en  la misma proporción que el sistema
de salud. Juan está contento porque con este trabajo pudo afiliarse a Cafam y
conoce bien los servicios de esta Caja de Compensación por la que solo tiene
que pagar `$5000` cuando ingrese a las instalaciones ubicadas en la autopista
norte.

Juan recibe un  subsidio  por  educación  equivalente  al  `5%` de  su  salario
bruto.  Conociendo  los  datos  del trabajo de Juan de una determinada semana,
calcule el valor neto que recibirá Juan.

## Refinamiento 1

1. Obtener de número de horas trabajadas.
1. Obtener costo de hora laboral.
1. Obtener cantidad de visitas a Cafam.
1. Calcular salario bruto.
1. Descontar aporte de empleado a salud (`-4.5%` sobre salario bruto).
1. Descontar aporte de empleado a pensión (`-3.5%` sobre salario bruto).
1. Descontar aporte de empleado por visita a caja de compensación (`-$5000`).
1. Incrementar subsidio de educación (`+5%` sobre salario bruto).
1. Imprimir en pantalla salario neto.

## Refinamiento 2

1. Declaro variable entera positiva `horas`.
1. Declaro variable entera positiva `costo`.
1. Declaro variable entera positiva `visitas`.
1. Pregunto a usuario por cantidad de horas trabajadas.
1. Obtengo de usuario un número entero positivo y asigno a variable `horas`.
1. Pregunto a usuario por costo de hora.
1. Obtengo de usuario un número entero positivo y asigno a variable `costo`.
1. Pregunto a usuario por visitas.
1. Obtengo de usuario un número entero positivo y asigno a variable `visitas`.
1. Declaro variable entera positiva `salBru` y asigno la multiplicación entre
   `horas` y `costo`.
1. Declaro variable entera positiva `salNet` y asigno el valor de `salBru`.
1. Resto a `salNet` el `4.5%` de `salBru` (salud).
1. Resto a `salNet` el `3.5%` de `salBru` (pensión).
1. Resto a `salNet` `5000 * visitas` (caja a compensación).
1. Sumo a `salNet` el `5%` de `salBru` (salud).
1. Imprimo en pantalla el valor de `salNet`.

# Referencias

[^1]: R. Muller, «A007953 - Digital sum (i.e., sum of digits) of n; also called
  digsum(n).» https://oeis.org/A007953, 2022. 
